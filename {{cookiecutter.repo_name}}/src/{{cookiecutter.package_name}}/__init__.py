""" {{ cookiecutter.project_short_description }} """

__version__ = "{{ cookiecutter.version }}"  # NOTE Use `bump2version patch` to bump versions correctly
